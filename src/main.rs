fn ftoc(f: f32) -> f32 {
    let c = (f-32.0)*(5.0/9.0);
    c
}

fn main() {
    let f = 30.0;
    let c = ftoc(f);

    println!("F of {} = C of {}", f, c);
}
